#! /bin/bash

# Env vars must be escaped before they're regexed
sed -i "s/ACCESS_KEY_TARGET/$( echo $ACCESS_KEY | sed -e 's/[\/&]/\\&/g' )/g" s3cmd.config
sed -i "s/SECRET_KEY_TARGET/$( echo $SECRET_KEY | sed -e 's/[\/&]/\\&/g' )/g" s3cmd.config
sed -i "s/HOST_TARGET/$( echo $HOST_NAME | sed -e 's/[\/&]/\\&/g' )/g" s3cmd.config
sed -i "s/SPACE_TARGET/$( echo $SPACE_NAME | sed -e 's/[\/&]/\\&/g' )/g" s3cmd.config
