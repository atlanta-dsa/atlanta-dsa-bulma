#! /bin/bash

releaseTagRegex='^([0-9]+\.[0-9]+\.[0-9]+)$'

pathPrefix="revel-bulma"
initialPath="unmatched"
path="$initialPath"
deployStable="false"

tag=$CI_COMMIT_TAG
branch=$CI_COMMIT_BRANCH

#tag='1.2.3'
#branch='develop'

if [[ $tag =~ $releaseTagRegex ]]; then
	path="stable"
elif [[ $branch == "develop" ]]; then
	path="edge"
elif [[ $CI_PIPELINE_SOURCE == 'merge_request_event' && $CI_MERGE_REQUEST_ID ]]; then
	path="merge/$CI_MERGE_REQUEST_ID"
fi

if [[ $path == "$initialPath" ]]; then
	echo "Categorization failed!"
	exit 1
fi

echo "Deploying to s3://$SPACE_NAME/dsa-bulma/$path/"

s3cmd -c s3cmd.config --acl-public sync dist/ "s3://$SPACE_NAME/dsa-bulma/$path/"
